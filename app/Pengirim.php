<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengirim extends Model
{
    protected $table = "pengirim";

    protected $fillable = [
        'nama_pengirim','alamat_pengirim','no_telp_pengirim','update_at','create_at'
    ];
}
