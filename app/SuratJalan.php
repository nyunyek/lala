<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuratJalan extends Model
{
    protected $table = "surat_jalan";

    protected $fillable = [
        'nama_penerima','alamat_penerima','no_telp_penerima','tanggal_buat','kode','jumlah_ongkos','keterangan','update_at','create_at'
    ];
}
