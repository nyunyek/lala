<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengirim;

class TransisiController extends Controller
{
    public function tambah_pengirim_view(){
        return view('manage.pengirim.tambah_pengirim');
    }
    public function tambah_surat_jalan()
    {
        $data = Pengirim::orderBy('nama_pengirim','asc')->get();
        return view('manage.suratJalan.tambah',['data'=>$data]);
    }
}
