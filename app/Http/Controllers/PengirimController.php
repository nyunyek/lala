<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengirim;

class PengirimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pengirim::orderBy('nama_pengirim','asc')->get();
        return view('manage.pengirim.index',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Pengirim::insert([
            'nama_pengirim' => $request->nama_pengirim,
            'alamat_pengirim' => $request->alamat_pengirim,
            'no_telp_pengirim' => $request->no_telp_pengirim
        ]);

        return redirect('pengirim');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Pengirim::find($id);
        return view('manage.pengirim.edit_pengirim',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Pengirim::find($id);
         $data->nama_pengirim = $request->get('nama_pengirim');
         $data->alamat_pengirim = $request->get('alamat_pengirim');
         $data->no_telp_pengirim = $request->get('no_telp_pengirim');
         $data->save();

         return redirect('pengirim');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengirim = Pengirim::find($id);
        $pengirim->delete();
        return redirect('pengirim');
    }
}
