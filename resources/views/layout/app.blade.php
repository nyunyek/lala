<!DOCTYPE html>
<html lang="en">
<head>

  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Rendang</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('modules/fontawesome/css/all.min.css') }}">


  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
  <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">

  <!-- CSS Libraries -->
  @stack('CSS')

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
  {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}
  <link rel="stylesheet" href="{{ asset('css/components.css') }}">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>

{{--
<style>
  .modal-backdrop{
    display: none;
  }
  .modal {
    background: rgba(0, 0, 0, 0.5);
    z-index: 9999999999;
  }

</style> --}}
<!-- /END GA --></head>


<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">

        @include('componen.header')

        @include('componen.sidebar')

        <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <div class="section-header">

                    @yield('title')

                </div>

                <div class="section-body">

                    @yield('content')

                </div>
            </section>

            {{-- @stack('modal') --}}
        </div>


        @include('componen.footer')

      </div>
  </div>

  {{-- @stack('script') --}}

  <!-- General JS Scripts -->
  {{-- <script src="{{ asset('js/app.js') }}"></script> --}}

  <script src="{{ asset('modules/jquery.min.js') }}"></script>
  <script src="{{ asset('modules/popper.js') }}"></script>
  <script src="{{ asset('modules/tooltip.js') }}"></script>
  <script src="{{ asset('js/scripts.js') }}"></script>
  <script src="{{ asset('modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('modules/moment.min.js') }}"></script>
  <script src="{{ asset('js/stisla.js')}}"></script>



  <!-- JS Libraies -->

  <!-- Page Specific JS File -->

  <!-- Template JS File -->
  {{-- <script src="{{ asset('js/custom.js') }}"></script> --}}

  @stack('script')
</body>
</html>
