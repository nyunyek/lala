@extends('layout.app')

@push('CSS')
{{-- <link rel="stylesheet" href="assets/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
<link rel="stylesheet" href="assets/modules/prism/prism.css"> --}}
<link rel="stylesheet" href="{{ asset('modules/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{ asset('modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css')}}">
{{-- <link href="/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> --}}
<link rel="stylesheet" href="{{ asset('modules/prism.css')}}">
@endpush

@section('title')
    <h1>Home</h1>
@endsection

