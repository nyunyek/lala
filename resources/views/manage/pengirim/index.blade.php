@extends('layout.app')

@push('CSS')
<link rel="stylesheet" href="{{ asset('modules/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{ asset('modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css')}}">}
<link rel="stylesheet" href="{{ asset('modules/prism.css')}}">
@endpush

@section('title')
    <h1>Pengirim</h1>
@endsection

@section('content')
<div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
                    <div align="right">
                        <a href="tambah_pengirim_view"><button class="btn btn-primary"> Tambah Pegirim<i class="fa fa-document-plus"></i></button></a>
                        </div>
                        <br>
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead class="text-center">
                    <tr>
                        <th>Nama Pengirim</th>
                        <th>Alamat Pengirim</th>
                        <th>No. Telp</th>
                        <th colspan="2">Action</th>
                    </tr>
                  </thead>
                  <tbody class="text-center" >

                        @foreach ($data as $i )
                          <tr>
                          <td>{{$i->nama_pengirim}}</td>
                          <td>{{$i->alamat_pengirim}}</td>
                          <td>{{$i->no_telp_pengirim}}</td>
                          <td>
                              <form action="{{ route('pengirim.destroy',$i->id)}}" method="POST">
                                <a href="{{ route('pengirim.edit',$i->id)}}" class="btn btn-icon btn-primary"><i class="far fa-edit">Edit</i></a>
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">-</button>
                            </form>

                            </td>
                          </tr>

                        @endforeach
                      </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@push('modal')

@endpush

@push('script')

  <script src="{{ asset('modules/datatables/datatables.min.js')}}"></script>
  <script src="{{ asset('modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{ asset('modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
  <script src="{{ asset('modules/jquery-ui/jquery-ui.min.js')}}"></script>

  <script src="{{ asset('modules/jquery-ui/components-table.js')}}"></script>
  <script src="{{ asset('modules/jquery-ui/jquery.min.js')}}"></script>
  <script src="{{ asset('js/page/modules-datatables.js')}}"></script>
  <script src="{{ asset('js/page/bootstap-modal.js')}}"></script>
  <script src="{{ asset('modules/prism/prism.js')}}"></script>
@endpush
