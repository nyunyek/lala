@extends('layout.app')

@section('title')
    <h1>Pengirim</h1>
@endsection

@section('content')
<div class="section-body">
    <h2 class="section-title">Tambah Pengirim</h2>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                        <div align="right">
                                <a href="pengirim"><button class="btn btn-primary"> Kembali<i class="fa fa-document-plus"></i></button></a>
                        </div>
                        <br>
                        <form action="{{route('pengirim.store')}}" method="POST">
                            @csrf
                        <div class="form-group">
                                <label>Nama Pengirim</label>
                                <input type="text" name="nama_pengirim" class="form-control">
                                <label>Alamat Pengirim</label>
                                <textarea class="form-control" name="alamat_pengirim" required=""></textarea>
                                <label>No. Telpon Pengirim</label>
                                <input type="text" name="no_telp_pengirim" class="form-control phone-number">
                                <div class="card-footer text-right">
                                        <button class="btn btn-primary">Simpan</button>
                                </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
