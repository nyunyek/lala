@extends('layout.app')

@section('title')
    <h1>Surat Jalan</h1>
@endsection

@section('content')
<div class="section-body">
        <h2 class="section-title">Edit Surat Jalan</h2>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div align="right">
                            <a href="/pengirim"><button class="btn btn-primary"> Kembali<i class="fa fa-document-plus"></i></button></a>
                    </div>
                    <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
