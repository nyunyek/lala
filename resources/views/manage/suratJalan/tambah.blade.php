@extends('layout.app')

@section('title')
    <h1>Surat Jalan</h1>
@endsection

@section('content')
    <div class="section-body">
        <h2 class="section-title">Buat Surat Jalan</h2>

        <div class="row">
            <div class="col-12">
                <div class="card">
                        <div align="right">
                                <a href="surat_jalan"><button class="btn btn-primary"> Kembali<i class="fa fa-document-plus"></i></button></a>
                        </div>
                        <div>
                            <form action="{{route('surat_jalan.store')}}" method="POST" class="needs-validation" novalidate="">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                            <label>Pilih Pengirim</label>
                                        <select class="form-control select2">
                                                @foreach ($data as $i)
                                          <option value="{{$i->id}}">{{$i->nama_pengirim}}</option>
                                          @endforeach
                                          {{-- <option>Option 2</option>
                                          <option>Option 3</option> --}}
                                        </select>

                                      </div>
                                    <div class="form-group">
                                            <label>Nama Penerima</label>
                                            <input type="text" name="nama_penerima" class="form-control" required="">
                                            <div class="invalid-feedback">
                                                Harus Diisi!
                                            </div>
                                    </div>
                                     <div class="form-group">
                                            <label>Alamat Penerima</label>
                                            <textarea class="form-control" name="alamat_penerima" required=""></textarea>
                                            <div class="invalid-feedback">
                                                Harus Diisi!
                                            </div>
                                    </div>
                                    <div class="form-group">
                                            <label>No. Telpon Penerima</label>
                                            <div class="input-group">
                                                    <div class="input-group-prepend">
                                                      <div class="input-group-text">
                                                        <i class="fas fa-phone"></i>
                                                      </div>
                                                    </div>
                                                <input type="text" name="no_telp_penerima" class="form-control phone-number">
                                            </div>
                                    </div>
                                    <div class="form-group">
                                        <table class="table" id="dynamic_field">
                                            <thead class="text-center">
                                                <tr>
                                                    <th>Nama Barang</th>
                                                    <th>Coli</th>
                                                    <th>Berat</th>
                                                    <th>Volume</th>
                                                    <th> </th>
                                                </tr>
                                                <tbody>

                                                </tbody>
                                               </thead>
                                        </table>
                                    </div>

                                    <div class="form-group">
                                            <label>Jumlah Ongkos</label>
                                            <input type="text" name="jumlah_ongkos" class="form-control">
                                    </div>
                                    <div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea name="keterangan" class="form-control"></textarea>
                                    </div>
                                    <div class="card-footer text-right">
                                            <button class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
<script>
    $(document).ready(function(){

     var count = 1;

     dynamic_field(count);

     function dynamic_field(number)
     {
      html = '<tr>';
            html += '<td><input type="text" name="first_name[]" class="form-control" /></td>';
            html += '<td><input type="text" name="last_name[]" class="form-control" /></td>';
            html += '<td><input type="text" name="last_name[]" class="form-control" /></td>';
            html += '<td><input type="text" name="last_name[]" class="form-control" /></td>';
            if(number > 1)
            {
                html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove">-</button></td></tr>';
                $('tbody').append(html);
            }
            else
            {
                html += '<td><button type="button" name="add" id="add" class="btn btn-success">+</button></td></tr>';
                $('tbody').html(html);
            }
     }

     $(document).on('click', '#add', function(){
      count++;
      dynamic_field(count);
     });

     $(document).on('click', '.remove', function(){
      count--;
      $(this).closest("tr").remove();
     });

     $('#dynamic_form').on('submit', function(event){
            event.preventDefault();
            $.ajax({
                url:'{{ route("dynamic-field.insert") }}',
                method:'post',
                data:$(this).serialize(),
                dataType:'json',
                beforeSend:function(){
                    $('#save').attr('disabled','disabled');
                },
                success:function(data)
                {
                    if(data.error)
                    {
                        var error_html = '';
                        for(var count = 0; count < data.error.length; count++)
                        {
                            error_html += '<p>'+data.error[count]+'</p>';
                        }
                        $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
                    }
                    else
                    {
                        dynamic_field(1);
                        $('#result').html('<div class="alert alert-success">'+data.success+'</div>');
                    }
                    $('#save').attr('disabled', false);
                }
            })
     });

    });
    </script>
@endpush

@section('script')
<script src="{{ asset('modules/jquery-selectric/jquery.selectric.min.js')}}"></script>
<script src="{{ asset('modules/cleave-js/dist/addons/cleave-phone.us.js')}}"></script>
<script src="{{ asset('modules/select2/dist/js/select2.full.min.js')}}"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

@endsection
