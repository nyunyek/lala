@extends('layout.app')

@push('CSS')
<link rel="stylesheet" href="{{ asset('modules/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{ asset('modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('modules/prism.css')}}">
@endpush

@section('title')
    <h1>Surat Jalan</h1>
@endsection

@section('content')
<div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
                    <div align="right">
                        <a href="tambah_surat_jalan_view"><button class="btn btn-primary"> Tambah Surat Jalan<i class="fa fa-document-plus"></i></button></a>
                        </div>
                        <br>
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead class="text-center">
                    <tr>
                        <th>Pengirim</th>
                        <th>Penerima</th>
                        <th>Tanggal Buat</th>
                        <th colspan="2">Action</th>
                    </tr>
                  </thead>
                  <tbody class="text-center" >
                        @foreach ($data as $i )
                          <tr>
                          <td>Nama : 'data'
                            <br>
                            Alamat : 'data'
                          </td>
                          <td>Nama : {{$i->nama_penerima}}
                            <br>
                            alamat : {{$i->alamat_penerima}}
                        </td>
                          <td>{{$i->tanggal_buat}}</td>
                          <td>
                            <a href="{{ route('surat_jalan.edit',$i->id)}}" class="btn btn-icon btn-primary"><i class="far fa-edit">Edit</i></a>
                            <a href="{{ route( 'surat_jalan.show', $i->id)}}" class="btn btn-secondary">Detail</a>
                            </td>
                          </tr>

                        @endforeach
                      </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@push('script')

  <script src="{{ asset('modules/datatables/datatables.min.js')}}"></script>
  <script src="{{ asset('modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{ asset('modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
  <script src="{{ asset('modules/jquery-ui/jquery-ui.min.js')}}"></script>

  <script src="{{ asset('modules/jquery-ui/components-table.js')}}"></script>
  <script src="{{ asset('modules/jquery-ui/jquery.min.js')}}"></script>
  <script src="{{ asset('js/page/modules-datatables.js')}}"></script>
  <script src="{{ asset('js/page/bootstap-modal.js')}}"></script>
@endpush
