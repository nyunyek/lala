@extends('layout.app')

@section('title')
    <h1>Surat Jalan</h1>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">}
                <div class="card-body table-responsive">
                    <div class="tab-pane fade show active">
                            <img src="/img/logo.png" alt="" style=" float:left; width:180px;height:180px;">
                        <h5>CV. EKA EXPRESS
                            <br>"CEPAT... TEPAT... AKURAT."</br>
                          </h5>
                          <h6>Office    :Jl. Sidotopo Lor No. 42 Surabaya
                            <br>Phone   :031-3763816
                            <br>Mobile  :08123374950, 081234641855
                            <br>Email   :ekaexpress1@gmail.com
                            <br>Webside :www.ekaexpress.com
                          </h6>
                          <br>
                          <br>
                          {{-- @foreach ($data as $i) --}}
                          <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td>Tanggal : {{$jalan->tanggal_buat}} </td>
                                    <td>No :  </td>
                                </tr>
                            </table>
                          </div>
                          <div class="table-responsive">
                                <table class="table table-striped" border="1">
                                        <thead class="text-center">
                                            <tr>
                                                <th>Pengirim</th>
                                                <th>Penerima</th>
                                                <th>Barang</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <td>
                                                Nama Pengirim : 'data'
                                                <br>
                                                Alamat Pengrirm : 'data'
                                                <br>
                                                No Telp Pengirim :
                                            </td>
                                            <td>
                                                Nama Penerima : {{$jalan->nama_penerima}}
                                                <br>
                                                Alamat Peneima : {{$jalan->alamat_penerima}}
                                                <br>
                                                No Telp Penerima :
                                            </td>
                                            <td>

                                            </td>
                                        </tbody>
                                        <thead class="text-center">
                                            <tr>
                                                <td>Rp @</td>
                                                <td>Jumlah Ongkos</td>
                                                <td>Keterangan</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                </table>
                          </div>
                          <table class="table table-striped">
                              <thead class="text-center">
                                  <tr>
                                      <td>Pengirim</td>
                                      <td>Penerima</td>
                                      <td>CV. EKA EXPRESS</td>
                                  </tr>
                              </thead>
                              <tbody class="text-center">
                                  <tr>
                                      <td>
                                          <br>
                                          <br>
                                          <br>
                                          ..........................................
                                          <br>
                                          'data'
                                      </td>
                                      <td>
                                          <br>
                                          <br>
                                          <br>
                                          .........................................
                                          <br>
                                          {{$jalan->alamat_penerima}}
                                      </td>
                                  </tr>
                              </tbody>
                          </table>
                          1.Putih(Pembawa) 2.Merah(Pengirim) 3.Kuning(penerima) 4.Hijau(Arsip)
                    </div>
                    <div class="pl-4">
                            <a href="/surat_jalan" class="float-right">
                                <button class="btn btn-default"><i class="fas fa-undo-alt"></i> Kembali</button>
                            </a>
                            <a href="/print_surat/{{$jalan->id}}" class="float-right">
                                    <button class="btn btn-default"><i class="fas fa-undo-alt"></i> Print</button>
                            </a>
                        </div>
                </div>
                {{-- @endforeach --}}
            </div>
        </div>
    </div>
</div>

@endsection
