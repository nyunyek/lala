@extends('layout.app')

@section('title')
    <h1>Invoice</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="tab-pane fade show active">
                            <div class="invoice">
                                    <div class="invoice-print">
                                      <div class="row">
                                        <div class="col-lg-12">
                                          <div class="invoice-title">
                                            <h2>Invoice</h2>
                                            <div class="invoice-number">Nomor Pesanan : 12345</div>
                                          </div>
                                          <hr>
                                          <div class="row">
                                            <div class="col-md-6">
                                              <address>
                                                <strong>Tagihan Untuk :</strong><br>
                                                  Nama Customer<br>
                                                  Alamat Customer<br>
                                                  No telp<br>
                                              </address>
                                            </div>
                                            <div class="col-md-6 text-md-right">
                                                <address>
                                                  <strong>Order Date:</strong><br>
                                                  September 19, 2018<br><br>
                                                </address>
                                              </div>
                                          </div>
                                          <div class="row">

                                          </div>
                                        </div>
                                      </div>

                                      <div class="row mt-4">
                                        <div class="col-md-12">
                                          <div class="section-title">Detail Pesanan</div>
                                          <div class="table-responsive">
                                            <table class="table table-striped table-hover table-md">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Nama Barang</th>
                                                        <th class="text-center">Jumlah Coli</th>
                                                        <th class="text-center">Harga per berat/volume</th>
                                                        <th class="text-right">Jumlah Harga</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-center">Mouse Wireless</td>
                                                        <td class="text-center">200</td>
                                                        <td class="text-center">$10.99</td>
                                                        <td class="text-right">$10.99</td>
                                                    </tr>
                                                </tbody>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>Total : {{}}</td>
                                                </tr>
                                            </table>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                    </div>
                </div>
                <div class="pl-4">
                        <a href="/invoice" class="float-right">
                            <button class="btn btn-default"><i class="fas fa-undo-alt"></i> Kembali</button>
                        </a>
                        <a href="#" class="float-right">
                                <button class="btn btn-default"><i class="fas fa-undo-alt"></i> Print</button>
                        </a>
                    </div>
            </div>
        </div>
    </div>
@endsection
