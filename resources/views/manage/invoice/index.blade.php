@extends('layout.app')

@push('CSS')
<link rel="stylesheet" href="assets/modules/datatables/datatables.min.css">
<link rel="stylesheet" href="assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">
<link rel="stylesheet" href="assets/modules/prism/prism.css">
@endpush


@section('title')
    <h1>Invoice</h1>
@endsection

@section('content')
<div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
                    <div align="right">
                            <a href="tambah_pengirim_view"><button class="btn btn-primary"> Tambah Invoice<i class="fa fa-document-plus"></i></button></a>
                        </div>
                        <br>
              <div class="table-responsive table-invoice">
                <table class="table table-striped" id="table-1">
                  <thead class="text-center">
                    <tr>
                            <th>No Pemesanan</th>
                            <th>Pengirim</th>
                            <th>Tanggal Pemesanan</th>
                            <th>Action</th>
                    </tr>
                  </thead>
                  <tbody class="text-center" >

                        {{-- @foreach ($data as $i ) --}}
                        <tr>
                                <td>87239</td>
                                <td class="font-weight-600">Kusnadi</td>
                                <td>July 19, 2018</td>
                                <td>
                                  <a href="{{route('invoice.show',1)}}" class="btn btn-primary">Detail</a>
                                </td>
                              </tr>

                        {{-- @endforeach --}}
                      </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@push('script')

  <script src="{{ asset('modules/datatables/datatables.min.js')}}"></script>
  <script src="{{ asset('modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{ asset('modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
  <script src="{{ asset('modules/jquery-ui/jquery-ui.min.js')}}"></script>

  <script src="{{ asset('modules/jquery-ui/components-table.js')}}"></script>
  <script src="{{ asset('modules/jquery-ui/jquery.min.js')}}"></script>
  <script src="{{ asset('js/page/modules-datatables.js')}}"></script>
  <script src="{{ asset('js/page/bootstap-modal.js')}}"></script>


@endpush
