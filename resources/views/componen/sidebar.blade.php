<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="\">CV. EKA EXPRESS</a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="\">E</a>
      </div>
      <ul class="sidebar-menu">
        {{-- <li class="menu-header">Dashboard</li> --}}
        <li class="dropdown">
          <a href="\" ><i class="fas fa-fire"></i><span>Home</span></a>
          {{-- <ul class="dropdown-menu">
            <li><a class="nav-link" href="index-0.html">General Dashboard</a></li>
            <li><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
          </ul> --}}
        </li>
        {{-- <li class="menu-header">Surat Jalan</li> --}}
        <li class="dropdown">
          <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>Surat Jalan</span></a>
          <ul class="dropdown-menu">
            <li><a class="nav-link" href="/pengirim">Daftar Pengirim</a></li>
            {{-- <li><a class="nav-link" href="/tambah_surat_jalan_view">Tambah Surat Jalan</a></li> --}}
            <li><a class="nav-link" href="/surat_jalan">Daftar Surat Jalan</a></li>
          </ul>
        </li>
        {{-- <li class="menu-header">Activity</li> --}}
        <li class="dropdown">
          <a href="#" class="nav-link has-dropdown"><i class="fas fa-columns"></i> <span>Invoice</span></a>
          <ul class="dropdown-menu">
            <li><a class="nav-link" href="invoice">Daftar Invoice</a></li>
          </ul>
        </li>
        {{-- <li class="menu-header">Report</li>
        <li class="dropdown">
          <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i> <span>Laporan</span></a>
          <ul class="dropdown-menu">
            <li><a class="nav-link" href="/report/siswa">Kehadiran Siswa</a></li>
            <li><a class="nav-link" href="#">Kehadiran Guru</a></li>
          </ul>
        </li> --}}
      </ul>

      {{-- <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
        <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
          <i class="fas fa-rocket"></i> Documentation
        </a>
      </div>         --}}
    </aside>
  </div>
