<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::resource('pengirim', 'PengirimController');

Route::resource('surat_jalan', 'SuratJalanController');

Route::resource('invoice', 'InvoiceController');

Route::get('tambah_pengirim_view', 'transisiController@tambah_pengirim_view');

Route::get('tambah_surat_jalan_view', 'transisiController@tambah_surat_jalan');

Route::get('/dinamik', 'transisiController@dinamik');

Route::post('dynamic-field/insert', 'DynamicFieldController@insert')->name('dynamic-field.insert');
